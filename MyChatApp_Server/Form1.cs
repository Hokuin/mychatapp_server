﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace MyChatApp_Server
{
    public partial class Server : Form
    {
        private static List<HandleClient> handleclients = new List<HandleClient>();

        public Server()
        {
            InitializeComponent();

            IPAddress[] localIP = Dns.GetHostAddresses(Dns.GetHostName());
            foreach (var address in localIP)
            {
                if (address.AddressFamily == AddressFamily.InterNetwork)
                    serverIP.Text = address.ToString();
            }
            serverIP.Enabled = false;
            FormClosing += new FormClosingEventHandler((Object sender, FormClosingEventArgs e) => { Environment.Exit(0); });
        }

        private void Start_Click(object sender, EventArgs e)
        {
            ClientScanner.RunWorkerAsync();
            ClientScanner.WorkerSupportsCancellation = true;

            Start.Enabled = false;
            serverPort.Enabled = false;
        }

        private void ClientScanner_DoWork(object sender, DoWorkEventArgs e)
        {
            TcpListener serverSocket = new TcpListener(IPAddress.Any, int.Parse(serverPort.Text));
            TcpClient clientSocket = default(TcpClient);
            serverSocket.Start();
            
            while (true)
            {
                clientSocket = serverSocket.AcceptTcpClient();
                HandleClient client = new HandleClient();
                handleclients.Add(client);
                client.StartClient(clientSocket);
            }
            /*
            clientSocket.Close();
            serverSocket.Stop();*/
        }

        private static void SendData(byte[] data, NetworkStream stream)
        {
            int bufferSize = 1024;

            byte[] dataLength = BitConverter.GetBytes(data.Length);

            stream.Write(dataLength, 0, 4);

            int bytesSent = 0;
            int bytesLeft = data.Length;

            while (bytesLeft > 0)
            {
                int curDataSize = Math.Min(bufferSize, bytesLeft);

                stream.Write(data, bytesSent, curDataSize);

                bytesSent += curDataSize;
                bytesLeft -= curDataSize;
            }
        }

        public static void SendToAll(byte[] bytesTo)
        {
            List<HandleClient> clientsWorkCopy = new List<HandleClient>(handleclients);
            foreach (var item in clientsWorkCopy)
            {
                try
                {
                    NetworkStream networkStream = item.clientSocket.GetStream();
                    SendData(bytesTo, networkStream);
                }
                catch (Exception)
                {
                    item.StopClient();
                    handleclients.Remove(item);
                }
            }
        }

        private class HandleClient
        {
            public TcpClient clientSocket;
            Thread clientThreadClientToServer;

            public void StartClient(TcpClient inClientSocket)
            {
                this.clientSocket = inClientSocket;
                clientThreadClientToServer = new Thread(Chat);
                clientThreadClientToServer.Start();
            }

            public void StopClient()
            {
                clientThreadClientToServer.Abort();
            }

            private void Chat()
            {
                while (true)
                {
                    try
                    {
                        byte[] bytesFrom = GetData();
                        SendToAll(bytesFrom);
                    }
                    catch (Exception)
                    {
                    }
                }
            }

            private byte[] GetData()
            {
                NetworkStream stream = clientSocket.GetStream();

                byte[] fileSizeBytes = new byte[4];
                int bytes = stream.Read(fileSizeBytes, 0, 4);
                int dataLength = BitConverter.ToInt32(fileSizeBytes, 0);

                int bytesLeft = dataLength;
                byte[] data = new byte[dataLength];

                int bufferSize = 1024;
                int bytesRead = 0;

                while (bytesLeft > 0)
                {
                    int curDataSize = Math.Min(bufferSize, bytesLeft);
                    if (clientSocket.Available < curDataSize)
                        curDataSize = clientSocket.Available;

                    bytes = stream.Read(data, bytesRead, curDataSize);

                    bytesRead += curDataSize;
                    bytesLeft -= curDataSize;
                }

                return data;
            }
        }
    }
}
